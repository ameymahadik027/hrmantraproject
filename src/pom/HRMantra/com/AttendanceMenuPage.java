package pom.HRMantra.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class AttendanceMenuPage 
{
	WebDriver web;
	
	public AttendanceMenuPage(WebDriver driver)
	{
		web = driver;
		PageFactory.initElements(web, this);
	}
	
	@FindBy(xpath=".//a[contains(text(), 'Monthly Time Sheet')][@id='mnu1052']")
	WebElement monthlyTimeSheetMenu;
	
	@FindBy(xpath=".//select[@id='cboPeriod']")
	WebElement dropDownMenu;

	@FindBy(xpath=".//input[@value='Show']")
	WebElement showBtn;
	
	@FindBy(xpath=".//iframe[@id='mainFrame']")
	WebElement mainFrameWin;
	
	@FindBy(xpath=".//img[@onclick='ShowNewFeatureDialog()']")
	WebElement whatsNewBtn;
	
	@FindBy(xpath=".//input[@id='ImageButton2']")
	WebElement closeBtn;
	
	@FindBy(xpath=".//button[@id='fullscreen']")
	WebElement fullScreenBtn;
	
	
	
	public void checkAttendanceForMonth() throws InterruptedException
	{
		web.switchTo().frame(mainFrameWin);
		Thread.sleep(2000);
		monthlyTimeSheetMenu.click();
		Thread.sleep(1000);
		Select sel = new Select(dropDownMenu);
		
		for(int i =1 ; i<=12; i++)
		{
			try	
			{
			sel.selectByIndex(i);
			Thread.sleep(1000);
			showBtn.click();
			Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("No Record Founds: "+ e);
			}
			web.close();
		}
	
	}
	
	
}

package pom.HRMantra.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeaveMenuPage 
{
	WebDriver web;
	
	@FindBy(xpath=".//a[contains(text(),'Leave Balance and History')]")
	WebElement leaveBalanceAndHistory;
	
	@FindBy(xpath=".//a[contains(text(),'Leave Application')]")
	WebElement leaveApplication;
	
	@FindBy(xpath=".//a[contains(text(),'Leave Approval')]")
	WebElement leaveApproval;

	@FindBy(xpath=".//a[contains(text(),'View Self Reports')]")
	WebElement viewSelfReports;

	@FindBy(xpath=".//iframe[@id='mainFrame']")
	WebElement mainFrameWin;
	
	@FindBy(xpath=".//input[@id='btnPrint']")
	WebElement printBtn;

	@FindBy(xpath=".//input[@id='chkIsCalcAsOnDate']")
	WebElement checkBoxBtn;

	@FindBy(xpath=".//input[@id='BtnShow']")
	WebElement showBtn;
	
	@FindBy(xpath=".//input[@id='dtpAsOnDate_BtnPickDate']")
	WebElement datePickerBtn;
	
	@FindBy(xpath=".//img[@onclick='ShowNewFeatureDialog()']")
	WebElement whatsNewBtn;
	
	@FindBy(xpath=".//input[@id='ImageButton2']")
	WebElement closeBtn;
	
	@FindBy(xpath=".//button[@id='fullscreen']")
	WebElement fullScreenBtn;
	
	
	
	public LeaveMenuPage(WebDriver driver)
	{
		web = driver;
		PageFactory.initElements(web, this);
		
	}

	public void leaveMenu() throws InterruptedException
	{
		web.switchTo().frame(mainFrameWin);
		Thread.sleep(1000);
		leaveBalanceAndHistory.click();
		Thread.sleep(3000);
		
	}
}


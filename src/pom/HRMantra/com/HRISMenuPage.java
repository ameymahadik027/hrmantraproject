package pom.HRMantra.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HRISMenuPage 
{
	WebDriver web;
	
	@FindBy(xpath=".//a[contains(text(), 'Resignation Application')]")
	WebElement resignationMenu;
	
	@FindBy(xpath=".//iframe[@id='mainFrame']")
	WebElement mainFrameWin;

	@FindBy(xpath=".//select[@id='CboExitReason']")
	WebElement mainExitReasonDropDown;

	@FindBy(xpath=".//input[@id='chklExitReasons_0']")
	WebElement otherExitReasonsCheckbox;

	@FindBy(xpath=".//input[@id='txtReason']")
	WebElement reasonInDetailsTextField;

	@FindBy(xpath=".//input[@id='BtnSaveAsDraft']")
	WebElement saveAsDraftBtn;
	
	@FindBy(xpath=".//input[@id='BtnSave']")
	WebElement submitBtn;
	
	@FindBy(xpath=".//input[@id='btnReset']")
	WebElement resetBtn;
	
	@FindBy(xpath=".//input[@id='btnDelete']")
	WebElement deleteBtn;
	
	@FindBy(xpath=".//img[@onclick='ShowNewFeatureDialog()']")
	WebElement whatsNewBtn;
	
	@FindBy(xpath=".//input[@id='ImageButton2']")
	WebElement closeBtn;
	
	@FindBy(xpath=".//button[@id='fullscreen']")
	WebElement fullScreenBtn;
	
	
	public HRISMenuPage(WebDriver driver) 
	{
		web=driver;
		PageFactory.initElements(web, this);
	}
	
	public void applicationForResignation() throws InterruptedException
	{
		web.switchTo().frame(mainFrameWin);
		Thread.sleep(2000);
		resignationMenu.click();
		Thread.sleep(1000);
		Select sel = new Select(mainExitReasonDropDown);
		sel.selectByIndex(1);
		Thread.sleep(2000);
		otherExitReasonsCheckbox.click();
		Thread.sleep(2000);
		reasonInDetailsTextField.sendKeys("Because, I did like additional compensation. As well as, I feel undervalued in your current role." + " I am looking for a new challenge." + " I want a job with better career growth opportunities.");
		Thread.sleep(2000);
		saveAsDraftBtn.click();
		
	}
	
}

package pom.HRMantra.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage 
{

	WebDriver web;
	
	@FindBy(xpath=".//input[@id='txtUserId']")
	WebElement username;
	
	@FindBy(xpath=".//input[@id='txtPassword']")
	WebElement password;
	
	@FindBy(xpath=".//input[@id='btnSubmit']")
	WebElement loginBtn;
	
	@FindBy(xpath=".//a[contains(text(), 'Submit Resume')]")
	WebElement submitResumeLink;

	
	
	public LoginPage(WebDriver driver) 
	{
		web= driver;
		PageFactory.initElements(web, this);
	}
	
	public void username()
	{
		username.sendKeys("FI1207");;
	}
	
	public void password()
	{
		password.sendKeys("");
	}
	
	public void loginLink()
	{
		loginBtn.click();
	}
}

package pom.HRMantra.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage 
{	
	WebDriver web;
	
	@FindBy(xpath=".//h4[contains(text(),'Attendance')]")
	WebElement attendanceMenu;	
	
	@FindBy(xpath=".//h4[contains(text(),'HRIS')]")
	WebElement hrisMenu;	
	
	@FindBy(xpath=".//h4[contains(text(),'PMS')]")
	WebElement pmsMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Leave')]")
	WebElement leaveMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Approve')]")
	WebElement approveMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Apply')]")
	WebElement applyMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Calendar')]")
	WebElement calendarMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Fav Links')]")
	WebElement favLinkMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Policy')]")
	WebElement policyMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Reports')]")
	WebElement reportsMenu;
	
	@FindBy(xpath=".//h4[contains(text(),'Social')]")
	WebElement socialMenu;
	
	@FindBy(xpath=".//img[@onclick='ShowNewFeatureDialog()']")
	WebElement whatsNewBtn;
	
	@FindBy(xpath=".//input[@id='ImageButton2']")
	WebElement closeBtn;
	
	@FindBy(xpath=".//button[@id='fullscreen']")
	WebElement fullScreenBtn;
	
	
	public DashboardPage(WebDriver driver)  // Parameterized constructor loading
	{
		web = driver;
		PageFactory.initElements(web, this);
	}
	
	
	public void attendanceMenufun() throws InterruptedException
	{
		attendanceMenu.click();
		Thread.sleep(2000);
	}
	
	public void hrisMenufun() throws InterruptedException
	{
		hrisMenu.click();
		Thread.sleep(2000);
	}
	
	public void pmsMenufun() throws InterruptedException
	{
		pmsMenu.click();
		Thread.sleep(2000);
	}
	
	public void leaveMenufun() throws InterruptedException
	{
		leaveMenu.click();
		Thread.sleep(2000);
	}
	
	public void approveMenufun() throws InterruptedException
	{
		approveMenu.click();
		Thread.sleep(2000);
	}
	
	public void applyMenufun() throws InterruptedException
	{
		applyMenu.click();
		Thread.sleep(2000);
	}
	
	public void calendarMenufun() throws InterruptedException
	{
		calendarMenu.click();
		Thread.sleep(2000);
	}
	
	public void favLinkMenufun() throws InterruptedException
	{
		favLinkMenu.click();
		Thread.sleep(2000);
	}
	
	public void policyMenufun() throws InterruptedException
	{
		policyMenu.click();
		Thread.sleep(2000);
	}
	
	public void reportsMenufun() throws InterruptedException
	{	
		reportsMenu.click();
		Thread.sleep(2000);
	}
	
	public void socialMenufun() throws InterruptedException
	{	
		socialMenu.click();
		Thread.sleep(2000);
	}
	
	public void whatsNewfun() throws InterruptedException
	{
		whatsNewBtn.click();
		Thread.sleep(2000);
		fullScreenBtn.click();
		Thread.sleep(1000);
		fullScreenBtn.click();
		Thread.sleep(1000);
		closeBtn.click();
	}
}

package test.HRMantra.com;

import java.util.Scanner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pom.HRMantra.com.AttendanceMenuPage;
import pom.HRMantra.com.DashboardPage;
import pom.HRMantra.com.HRISMenuPage;
import pom.HRMantra.com.LoginPage;

public class TestCaseExecution {
	
	WebDriver driver;
	
	@BeforeClass
	public void test() 
	{
		String path = System.getProperty("user.dir");
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the browser name:");
		String str = sc.next();
		
		if(str.equalsIgnoreCase("chrome"))
		{
		System.setProperty("webdriver.chrome.driver", path + "/DriverFiles/NewChromeJsrFile/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		}
		else if(str.equalsIgnoreCase("firefox"))
		{
		System.setProperty("webdriver.gecko.driver", path + "/DriverFiles/NewChromeJsrFile/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		}
		else if(str.equalsIgnoreCase("Safari"))
		{
		System.setProperty("", path + "");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		}
		else
		{
		System.setProperty("", path + "");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		}
		
		driver.get("http://192.168.104.31/HRMLive/00_common/default.aspx");
		//driver.close();
		System.out.println(driver);
		
	}
	
	@Test(priority=0)
	public void login() throws InterruptedException
	{
		LoginPage log = new LoginPage(driver);
		Thread.sleep(2000);
		log.username();
		Thread.sleep(2000);
		log.password();
		Thread.sleep(2000);
		log.loginLink();
		Thread.sleep(2000);
		//driver.close();
	}

	@Test(priority=1)
	public void dashboardPage() throws InterruptedException
	{
		DashboardPage dbp =  new DashboardPage(driver);
		dbp.attendanceMenufun();
		dbp.hrisMenufun();
		dbp.whatsNewfun();
	}
	
	@Test(priority=2)
	public void attendanceMenuPage() throws InterruptedException
	{
		AttendanceMenuPage amp = new AttendanceMenuPage(driver);
		amp.checkAttendanceForMonth();
	}
	/*@Test(priority=3)
	public void hrisMenuPage() throws InterruptedException
	{
		HRISMenuPage hris = new HRISMenuPage(driver);
		hris.applicationForResignation();
	}*/
	
}
